import React from "react";

import LayoutStandardPage from "./../../layouts/layout-standard-page/layout-standard-page";

const ExperiencePage = (props: any) => {
  return (
    <LayoutStandardPage headerData={props.headerData} >
      <p>
        <a target="_blank" rel="noopener noreferrer" href="https://www.savium.co.uk/"> <b> Savium </b> </a> <br />
        Senior software engineer. Financial automation tool with open banking and crypto integrations
      </p>
      <p>
        <a target="_blank" rel="noopener noreferrer" href="https://it.auth.gr/"> <b> Aristotle University of Thessaloki IT Center (ITAUTH) </b> </a> <br />
        Web developer. Angular2 front end apps, Nodejs API with complex model schema 
      </p>
      <p>
        <a target="_blank" rel="noopener noreferrer" href="https://www.bearinmind.gr/"> <b> Bear in mind </b> </a> <br />
        Web developer. Wordpress custom plugins. Apps using ReactJS
      </p>
      <p>
        <a target="_blank" rel="noopener noreferrer" href="https://www.pointblank.gr/en"><b> PointBlank </b> </a> <br />
        Front-end Web developer. High end designs on Drupal 8 front-end and ReactJS apps
      </p>
      <p>
        <a target="_blank" rel="noopener noreferrer" href="https://summerofcode.withgoogle.com/"> <b> Google Summer Of Code </b> </a>  <br />
        <a target="_blank" rel="noopener noreferrer" href="https://github.com/etetoolkit/treematcher"> Etetoolkit/treematcher </a> tool. <br/>
        A searching tool for data trees using 
        regular expressions.
      </p>
    </LayoutStandardPage>
  );
};

export default ExperiencePage;