import React from "react";

import LayoutStandardPage from "./../../layouts/layout-standard-page/layout-standard-page";

const IndexPage = (props: any) => {
  return (
    <LayoutStandardPage headerData={props.headerData} title="@dpliakos">
      <p>
      Dimitrios is a full-stack web developer with four years of experience. 
      Dimitrios’ experience ranges from simple web/devops solutions to highly technical algorithmic
      tools and complex web systems following the full product lifecycle giving 
      him a great sense of balance between robustness, extensibility and practicality.
      </p>
      <p>
        Dimitrios is an active open source contributor via the Universis and the OpenSCN projects, 
        member of local tech communities and ocasional co-organizer and presenter of tech-events.
      </p>
    </LayoutStandardPage>
  );
};

export default IndexPage;


