import React from "react";

import LayoutStandardPage from "./../../layouts/layout-standard-page/layout-standard-page";

const OpenSourcePage = (props: any) => {
  return (
    <LayoutStandardPage headerData={props.headerData}>
      <h4> Contact Info </h4>
      <ul>
        <li> <a target="_blank" rel="noopener noreferrer" href="mailto:dimitrispl96@gmail.com"> dimitrispl96@gmail.com </a> </li>
        <li> <a target="_blank" rel="noopener noreferrer" href="mailto:dpliakos@it.teithe.gr"> dpliakos@it.teithe.gr </a> </li>
        <li> <a target="_blank" rel="noopener noreferrer" href="https://github.com/dPliakos/personal-webpage"> Github  </a></li>
        <li> <a target="_blank" rel="noopener noreferrer" href="https://gitlab.com/dPliakos"> Gitlab </a> </li>
        <li> <a target="_blank" rel="noopener noreferrer" href="https://twitter.com/Dimitris_Pl"> Twitter </a> </li>
      </ul>

      <h4> Community channels </h4>
      <ul>
        <li> <a target="_blank" rel="noopener noreferrer" href="https://chat.lambdaspace.gr/"> Lambdaspace's mattermost @chat.lambdaspace.gr </a> </li>
        <li> slack: OpenSCN </li>
        <li> slack: IEEE ATEITH SB</li>
        <li> slack: Drupal </li>
      </ul>
    </LayoutStandardPage>
  );
};

export default OpenSourcePage;

